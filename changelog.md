## 2015/10/31 DLV initial commit 0.0.1 unpublished
1. Initial commit

## 2015/10/31 DLV cleanup 0.0.1 unpublished
1. Cleaned up gulp to not copy gulpfile to dist directory
## 2015/11/01 DLV publish and tag 0.0.1 published
1. release initial version
## 2015/11/14 DLV 0.1.0 refactor to allow nested suites published
1. BREAKING CHANGE Data is now returned in heirarchal form using suites and nested suites.
## 2015/11/14 DLV 0.1.1 published
1. Terminate child process on JasmineDone



